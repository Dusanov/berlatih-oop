<?php

class Animal 
{
  public $name = "shaun";
  public $legs = 2;
  public $cold_blooded = "false";
  
  public function getName() {
  	return $this ->name;
  }

  public function cekInfo() {
  	return $this->name." ".$this->legs." ".$this->cold_blooded;
  }
  
}

?>