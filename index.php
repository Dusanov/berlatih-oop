<?php

include("animal.php");
include("frog.php");
include("ape.php");

$binatang = new Animal;
$kodok = new Buduk\Frog();
$sungokong = new KeraSakti\Ape();

echo $binatang->name;
echo "<br>";
echo $binatang->legs;
echo "<br>";
echo $binatang->cold_blooded;
echo "<br>";
echo $kodok->name;
echo "<br>";
echo $kodok->legs;
echo "<br>";
echo $sungokong->name;
echo "<br>";
echo $sungokong->legs;

?>